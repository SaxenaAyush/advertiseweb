import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { HomeScreenComponent } from './components/home-screen/home-screen.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import {CarouselModule} from 'ngx-owl-carousel-o';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmbedVideo } from 'ngx-embed-video';
import { HttpClientModule } from '@angular/common/http';
import { AdvertisePageComponent } from './components/advertise-page/advertise-page.component';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr-FR');
import { TimePickerModule } from 'ng-time-picker';
import { FooterComponent } from './components/footer/footer.component';
import { AdmindashboardComponent } from './components/admin/admindashboard/admindashboard.component';
import { ManageclintComponent } from './components/admin/manageclint/manageclint.component';
import { HeaderComponent } from './components/admin/header/header.component';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { ViewclientComponent } from './components/admin/viewclient/viewclient.component';
import { ManageCompanyComponent } from './components/admin/manage-company/manage-company.component';
import { AddcompanyComponent } from './components/admin/addcompany/addcompany.component';
import { AccountComponent } from './components/admin/account/account.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    HomeScreenComponent,
    DashboardComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    AdvertisePageComponent,
    FooterComponent,
    AdmindashboardComponent,
    ManageclintComponent,
    HeaderComponent,
    SidebarComponent,
    ViewclientComponent,
    ManageCompanyComponent,
    AddcompanyComponent,
    AccountComponent,
    ChangepasswordComponent,
    
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CarouselModule,
    BrowserAnimationsModule,
    EmbedVideo.forRoot(),
    HttpClientModule,
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    TimePickerModule
      ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
