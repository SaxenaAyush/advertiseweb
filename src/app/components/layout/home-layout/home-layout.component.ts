import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-layout',
  template: `

  <div>
  <router-outlet></router-outlet> 
  
  </div>
  

  `,
  styleUrls: ['./home-layout.component.scss']
})
export class HomeLayoutComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {

   
  }
}
