import { Component, OnInit } from '@angular/core';
import { EmbedVideoService } from 'ngx-embed-video';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  customOptions: any = {
    items: 1,
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    itemClass: 'media media-pointer',
    dots: false,
    navSpeed: 1500,
    nav: false,
    navigation: false,
    navText: ['<img src=\'assets/images/icons/backward45.png\'>', '<img src=\'assets/images/icons/forward45.png\'>'],
    autoplay: true,
    autoplayHoverPause: true
  };
  slidesStore = [
    {
      src: 'assets/images/banner-image.png',
      title: 'Lets create outstanding AdvertiseMent'
    },
    {
      src: 'assets/images/home-banner.jpg',
      title: 'Create your own Adds'
    },
    {
      src: 'assets/images/banner-img1.jpg',
      title: 'Its Our Promise'
    },
  ];
  slider = [
    {
      src: 'assets/images/art.png',
    },
    {
      src: 'assets/images/business.png',
    },
    {
      src: 'assets/images/calsses.png',
    },
    {
      src: 'assets/images/parties.png',
    },
    {
      src: 'assets/images/Music.png',
    },
    {
      src: 'assets/images/concert.png',
    },
    {
      src: 'assets/images/convention.png',
    },
    {
      src: 'assets/images/Dinner.png',
    },
    {
      src: 'assets/images/horses-race.png',
    },
    {
      src: 'assets/images/social-gathering.png',
    },
  ];
  vimeoUrl = 'https://vimeo.com/197933516';
  youtubeUrl = 'https://www.youtube.com/watch?v=iHhcHTlGtRs';
  dailymotionUrl =
    'https://www.dailymotion.com/video/x20qnej_red-bull-presents-wild-ride-bmx-mtb-dirt_sport';
 
  vimeoId = '197933516';
  youtubeId = 'iHhcHTlGtRs';
  dailymotionId = 'x20qnej';
 
  constructor(private embedService: EmbedVideoService,private router :Router) { }

  ngOnInit() {
  
  }

  home(){
this.router.navigate(['/home']);
  }
}
