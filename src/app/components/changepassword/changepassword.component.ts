import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  changepass(event: any){
    if(event.target.value == "pro"){
      this.router.navigate(['/changepassword']);
    }
    else if(event.target.value == "log")
    {
      this.router.navigate(['/dashboard']);
    }
  }
}
