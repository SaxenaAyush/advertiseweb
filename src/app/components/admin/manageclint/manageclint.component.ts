import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-manageclint',
  templateUrl: './manageclint.component.html',
  styleUrls: ['./manageclint.component.css']
})
export class ManageclintComponent implements OnInit {

  constructor( private router:Router ) { }

  ngOnInit() {
  }

  viewclient()
  {
    this.router.navigate(['/viewclient']);
  }

}
