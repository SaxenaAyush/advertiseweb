import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageclintComponent } from './manageclint.component';

describe('ManageclintComponent', () => {
  let component: ManageclintComponent;
  let fixture: ComponentFixture<ManageclintComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageclintComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageclintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
