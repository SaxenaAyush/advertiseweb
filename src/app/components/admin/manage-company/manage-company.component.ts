import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-manage-company',
  templateUrl: './manage-company.component.html',
  styleUrls: ['./manage-company.component.css']
})
export class ManageCompanyComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  addcomp()
  {
    this.router.navigate(['/addcompany']);

  }


}
