import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  variable:boolean = true;
  constructor() { }

  ngOnInit() {
  }
  onEdit() {
    if(this.variable == true) {
      this.variable = false;
    }
  }
}
