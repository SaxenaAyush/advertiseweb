import { Component, OnInit } from '@angular/core';
import {FileUploader} from 'ng2-file-upload';
import * as $ from '../../../../node_modules/jquery';
import { AdvertiseStorageService } from 'src/app/services/advertise-storage.service';
import * as moment from 'moment';
import {environment} from '../../../environments/environment';
import { Router } from '@angular/router';

import { from } from 'rxjs';

declare var Stripe: any;
@Component({
  selector: 'app-advertise-page',
  templateUrl: './advertise-page.component.html',
  styleUrls: ['./advertise-page.component.css']
})
export class AdvertisePageComponent implements OnInit {
  cartDetails: any;
  isCredit = false;
  isPhonePay = false;
  isPaytm = false;
  baseImgUrl = environment;
  stripe: any;
  elements: any;
  card: any;
  selectedCard: any;
  cardsList: any[] = [];
  public uploader: FileUploader = new FileUploader({});
  imageUrl: string | ArrayBuffer;
  imagePK:any;
  loggedInUserDetails: any;
  minDate = {year: parseInt(moment().format('YYYY')), month: parseInt(moment().format('MM')), day: parseInt(moment().format('DD'))};
 
  constructor(private storageService : AdvertiseStorageService, private router:Router) { 
    this.uploader.onAfterAddingFile = (file) => {
      const reader = new FileReader();
      reader.onload = (event) => {
        this.imageUrl = reader.result;
        console.log(reader.result)

        const imgData = {
          'detail': reader.result,
          'entityName': 'EOMenuItem',
          'type': file.file.type
        };
       console.log('image',imgData);
       
       this.storageService.set('imageData', imgData);
        // this.lotteryService.makeRequestApi('post', 'createImgObject', imgData).subscribe((res: any) => {
        //   this.imagePK = res.data.primaryKey;
        // }, err => {
        // });
      };
      console.log(reader.readAsDataURL(file._file));
    };
  }
  ngOnInit() {
    this.loggedInUserDetails = this.storageService.get('imageData');
      // if (this.loggedInUserDetails.eoImage !== null) {
      //   this.imageUrl = this.loggedInUserDetails.eoImage.imageUrl;
      // }
      this.stripe = Stripe(environment.stripeToken);
      this.elements = this.stripe.elements();
      const styles = {
        base: {
          color: '#32325d',
          fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
          fontSmoothing: 'antialiased',
          fontSize: '16px',
          '::placeholder': {
            color: '#aab7c4'
          }
        },
        invalid: {
          color: '#fa755a',
          iconColor: '#fa755a'
        }
      };
      this.card = this.elements.create('card', {style: styles});
      this.card.mount('#card-element');
      this.card.addEventListener('change', (event) => {
        const displayError = document.getElementById('card-errors');
        if (event.error) {
          displayError.textContent = event.error.message;
        } else {
          displayError.textContent = '';
        }
      });
    
    }
  
close(){
  $('.content').hide();
}
addUrl(){
  $('.content').show();
 
}
onNotify($event) { 
  console.log($event);
}

changepass(event: any){
  if(event.target.value == "pro"){
    this.router.navigate(['/changepassword']);
  }

  else if(event.target.value == "log"){
  this.router.navigate(['/dashboard']);
  }
}

 
}
