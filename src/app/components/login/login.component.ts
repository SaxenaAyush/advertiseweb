import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userList = [
    {
      id: 'admin',
      password: 12345
    },
    {
      id: 'user',
      password: 56789
    }
  ];
public loginForm: FormGroup
  constructor(private router: Router, private route: Router, private fb: FormBuilder,) { }

  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.loginForm = this.fb.group({
      emailID: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
login(){

  if (this.loginForm.valid) {
    const reqMap = {
      emailID: this.loginForm.value.emailID,
      password: this.loginForm.value.password
        };
        console.log("data",reqMap);
        const usr = this.userList.filter(usr => usr.id == this.loginForm.controls.emailID.value);
        const pass = this.userList.filter(pass => pass.password == this.loginForm.controls.password.value);
        if (usr.length > 0) {
          if (usr[0].id == 'admin' && pass[0].password == 12345) {
            this.router.navigate(['/admindashboard']);
            console.log("if working");
    
          } else if (usr[0].id == 'user' && pass[0].password == 56789) {
            this.router.navigate(['/advertise']);
            console.log("else working");
          }
        
        }  
        else{
          alert("User not available");
        }
        }
}
}





// export class LoginComponent implements OnInit {
// emailinput = "";
// passwordinput="";
// alertmsg = '';
//   constructor( private router:Router) { }

//   ngOnInit() {
//   }
//   ad() {
//     if(this.emailinput == "admin" && this.passwordinput== "12345" ){
//           this.router.navigate(['/admindashboard']);
//     }
//     else if(this.emailinput == "user@gmail.com" && this.passwordinput == "56789") {
//       // alert("USER")
//       this.router.navigate(['/advertise']);
//     }
//     else {
//       alert("User not available");
//     }
//   }
//   emailVerify(email){
//     if ((/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) || (email == 'admin') || (email == 'Admin')){
//       this.emailinput = email;
//       this.alertmsg = '';
//     }
//     else {
//       this.alertmsg = "Please enter valid emailId";
//     }
//   }
//   passwordverify(password){
//     // console.log(password)
//     this.passwordinput = password;

//   }
// }
