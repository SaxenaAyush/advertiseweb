import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdvertiseStorageService {

 
  constructor() {
    if (!this.get('advertise')) {
      localStorage.setItem('advertise', '');
    }
  }

  get(key?: string) {
    const val = JSON.parse(decodeURIComponent(escape(localStorage.getItem('advertise') ?
      atob(localStorage.getItem('advertise')) : '{}')));
    return val[key];
  }

  set(key: string, val: any) {
    const all = JSON.parse(decodeURIComponent(escape(localStorage.getItem('advertise') ?
      atob(localStorage.getItem('advertise')) : '{}')));
    all[key] = val;
    return localStorage.setItem('advertise', btoa(unescape(encodeURIComponent(JSON.stringify(all)))));
  }

  remove(key) {
    const all = JSON.parse(decodeURIComponent(escape(localStorage.getItem('advertise') ?
      atob(localStorage.getItem('advertise')) : '{}')));
    delete all[key];
    localStorage.setItem('advertise', btoa(unescape(encodeURIComponent(JSON.stringify(all)))));
    return true;
  }
}
