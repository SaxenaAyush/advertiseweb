import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Router } from '@angular/router';
import { ConstantsHelper } from '../constant/constant';
import {catchError, tap} from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AdvertiseHttpService {

  constructor(private http: HttpClient, private router: Router) { }
  public makeRequestApi(type,urlName,payload?,params?){
    const apiUrl = ConstantsHelper.getAPIUrl[urlName](params);
    switch (type) {
      case 'get':
        const headerOpt = payload ? {} : {
          headers: {
            apikey: 'Advertise@123'
          }
        };
        return this.http.get(apiUrl, headerOpt).pipe(
          tap((result: any) => {

          }),
          catchError(this.handleError())
        );
        case 'post':
          return this.http.post(apiUrl, JSON.stringify(payload))
         
            .pipe(
              tap((result: any) => {
              }),
              catchError(this.handleError())
            );
    }

  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      let errorMessage = null;
      if (error.error instanceof ErrorEvent) {
        errorMessage = 'An error occurred:' + ',' + error.error.message;
      } else {


        if (error.status === 400) {
          this.router.navigate(['/dashboard']);
        }
        if (error.status === 401) {
          this.router.navigate(['/dashboard']);
        }
        if (error.status === 500) {
     
        }
        errorMessage = `${error.error.message}`;
      }
      // this.log(`${operation} failed: ${error.message}`);
      // this.alertService.swalError();
      return throwError(errorMessage = errorMessage ? errorMessage : 'Something bad happened; please try again later.');
    };
  }
}
export class ApiInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Origin', '*').set('Content-Type', 'application/json').set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept').set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT').set('withCredentials', 'true') });
    // if (req.method != 'GET')
    req = req.clone({headers: req.headers.set('Content-Type', 'application/json')});
    // req = req.clone({
    //   headers: req.headers.set('Access-Control-Allow-Origin', '*').set('Content-Type', 'application/json').set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept').set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT')
    // })

    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept') });
    // req = req.clone({ headers: req.headers.set('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT') });
    // req = req.clone({ params: req.params.set('filter', 'completed') });

    return next.handle(req);
  }
}
