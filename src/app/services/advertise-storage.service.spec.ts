import { TestBed } from '@angular/core/testing';

import { AdvertiseStorageService } from './advertise-storage.service';

describe('AdvertiseStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiseStorageService = TestBed.get(AdvertiseStorageService);
    expect(service).toBeTruthy();
  });
});
