import { TestBed } from '@angular/core/testing';

import { AdvertiseHttpService } from './advertise-http.service';

describe('AdvertiseHttpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdvertiseHttpService = TestBed.get(AdvertiseHttpService);
    expect(service).toBeTruthy();
  });
});
