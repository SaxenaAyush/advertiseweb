import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginLayoutComponent } from './components/layout/login-layout/login-layout.component';
import { HomeLayoutComponent } from './components/layout/home-layout/home-layout.component';
import { HomeScreenComponent } from './components/home-screen/home-screen.component';
import { LoginComponent } from './components/login/login.component';
import { AdvertisePageComponent } from './components/advertise-page/advertise-page.component';
import { AdmindashboardComponent } from './components/admin/admindashboard/admindashboard.component';
import { ManageclintComponent } from './components/admin/manageclint/manageclint.component';
import { HeaderComponent } from './components/admin/header/header.component';
import { SidebarComponent } from './components/admin/sidebar/sidebar.component';
import { ViewclientComponent } from './components/admin/viewclient/viewclient.component';
import { combineAll } from 'rxjs/operators';
import { ManageCompanyComponent } from './components/admin/manage-company/manage-company.component';
import { AddcompanyComponent } from './components/admin/addcompany/addcompany.component';
import { AccountComponent } from './components/admin/account/account.component';
import { ChangepasswordComponent } from './components/changepassword/changepassword.component';


const routes: Routes = [

  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full'
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },

      {
        path: 'home',
        component: HomeScreenComponent
      },
      {
        path: 'login',
        component:LoginComponent
      },
      {
        path: 'advertise',
        component: AdvertisePageComponent
      },
      {
        path: 'admindashboard',
        component: AdmindashboardComponent
      },
      {
        path: 'manageclint',
        component: ManageclintComponent
      },
      {
        path: 'header',
        component: HeaderComponent
      },
      {
        path: 'sidebar',
        component: SidebarComponent
      },
      {
        path: 'viewclient',
        component: ViewclientComponent
      },
      {
        path: 'managecompany',
        component: ManageCompanyComponent
      },
      {
        path: 'addcompany',
        component: AddcompanyComponent
      },
      {
        path: 'account',
        component: AccountComponent
      },
      {
        path: 'changepassword',
        component: ChangepasswordComponent
      }
      
    ]
  },
  {
    path: 'client',
    component: HomeLayoutComponent,
   
    children: [
    
     
    ]
  },


  { path: '**', redirectTo: '', pathMatch: 'full' }

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
